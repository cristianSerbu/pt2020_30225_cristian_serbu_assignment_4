package controller;

import base.CompositeProduct;
import base.Order;
import model.WaiterModel;
import view.WaiterView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

public class WaiterCon {
    private WaiterModel theModel;
    private WaiterView theView;

    public WaiterCon(WaiterModel theModel, WaiterView theView) {
        this.theModel = theModel;
        this.theView = theView;
        ArrayList<Order> orders = new ArrayList<Order>();
        this.theView.add3Listener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayList<CompositeProduct> meniu = theModel.getMenu();
                int indexes[] =theView.getMenuItems();
                ArrayList<CompositeProduct> itemsOrdered= new ArrayList<CompositeProduct>();
                for(int i : indexes){
                    itemsOrdered.add(meniu.get(i));
                }
                Order order= theView.createOrder();
                theModel.addOrder(order,itemsOrdered);
                theView.addOrder(order);
                orders.add(order);
            }
        });
        this.theView.addpriceListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Collection<CompositeProduct> gotten = theModel.getKey(orders.get(theView.getOrder()));
                int total=0;
                for(CompositeProduct cp : gotten){
                    total+=cp.computePrice();
                }
                theView.setPret(total);
            }
        });
        this.theView.addGenereazaListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FileWriter fileWriter = null;
                try {
                    fileWriter = new FileWriter("chitanta.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                PrintWriter printWriter = new PrintWriter(fileWriter);
                StringBuilder builder = new StringBuilder();
                int total=0;
                for(CompositeProduct cp : theModel.getKey(orders.get(theView.getOrder()))){
                    total+=cp.computePrice();
                    builder.append(cp);
                }
                String items = builder.toString();
                printWriter.print("SC.Panini.SRL\n");
                printWriter.print("Comanda------------- " +orders.get(theView.getOrder()));
                printWriter.print("\nIteme:\n");
                printWriter.print(items);
                printWriter.print("\nPRET TOTAL: ");
                printWriter.printf("%d",total);
                printWriter.close();
            }
        });
        this.theView.addRefreshListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                theView.setList(theModel.getMenu());

            }
        });
        this.theView.setList(theModel.getMenu());
    }


}

