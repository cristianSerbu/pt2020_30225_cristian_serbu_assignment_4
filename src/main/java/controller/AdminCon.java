package controller;

import model.AdminModel;
import view.AdminView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminCon {
    private AdminView theView;
    private AdminModel theModel;

    public AdminCon(AdminView theView, AdminModel theModel) {
        this.theView = theView;
        this.theModel = theModel;

        this.theView.add3Listener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                theModel.addMenuItem(theView.getMenu());
                System.out.println(theModel.getMenu());
                theView.addToList(theView.getMenu());
            }
        });
        this.theView.addStergeListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                theModel.deleteMenuItem(theView.deleteFromList());
                System.out.println(theModel.getMenu());
            }
        });

        this.theView.addEditListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                theModel.setMenu(theView.getMenu(),theView.editFromList(theView.getMenu()));
                System.out.println(theModel.getMenu());
            }
        });

        this.theView.setList(theModel.getMenu());
    }

}
