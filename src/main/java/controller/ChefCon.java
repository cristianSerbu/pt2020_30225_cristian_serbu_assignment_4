package controller;

import base.OrderObserver;
import model.ChefModel;
import view.ChefView;

public class ChefCon {
    private ChefView theView;
    private ChefModel theModel;

    public ChefCon(ChefView theView, ChefModel theModel) {
        this.theView = theView;
        this.theModel = theModel;

        OrderObserver orderObserver = new OrderObserver(theView);
        theModel.getRestaurant().addObserver(orderObserver);
    }

}
