package base;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements java.io.Serializable {
    //ArrayList<CompositeProduct> menuItem= new ArrayList();
    private String name;
    private int price;

    public CompositeProduct(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public int computePrice(){

        return price;
    }

    @Override
    public String toString() {
        return "'" + name + '\'' +
                ", price=" + price+ "lei\n";
    }
}
