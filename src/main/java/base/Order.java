package base;

import java.util.Objects;

public class Order {
    private int id;
    private int date;
    private int table;

    public Order(int id, int date, int table) {
        this.id = id;
        this.date = date;
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                date == order.date &&
                table == order.table;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, table);
    }

    @Override
    public String toString() {
        return "Order " + id + ", table " + table + ", date(day) "+ date;
    }
}
