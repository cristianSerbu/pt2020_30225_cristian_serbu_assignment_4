package base;

public class BaseProduct extends MenuItem {

    private int name;
    private int price;

    public BaseProduct(int name, int price) {
        this.name = name;
        this.price = price;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int computePrice() {
        return price;
    }
}
