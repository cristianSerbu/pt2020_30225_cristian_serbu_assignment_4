package base;

import java.util.*;

public class Restaurant extends Observable implements  IRestaurantProcessing, java.io.Serializable{
    private ArrayList<CompositeProduct> menuItems;
    private HashMap<Order, Collection<CompositeProduct>> hashMap;
/**
 * constructor - should be well formed
 * @invariant isWellFormed()
 * */
    public Restaurant() {
        this.menuItems = new ArrayList<>();
        this.hashMap=new HashMap<>();
        assert isWellFormed();
    }
/**
 * returns the arraylist with the menu
 * @pre !isEmpty()
 * @post @nochange
 * @return the menu
 * */
    public ArrayList<CompositeProduct> getMenuItems() {
        assert !isEmpty();
        return menuItems;
    }

    private boolean isEmpty() {
        if(menuItems.size()!=0)
        return false;
        return true;
    }
/**
 * adds a new order to the hashmap
 * @pre @nochange
 * @post getSize()==getSize()@pre+1
* */
    public void addOrder(Order order,Collection<CompositeProduct> c){
        assert isWellFormed();
        int sizePre=hashMap.size();
        hashMap.put(order,c);
        setChanged();
        notifyObservers(c);
        int sizePost=hashMap.size();
        assert sizePost==sizePre+1;
        assert isWellFormed();
    }

    /**
     * adds an item to the menu
     * @pre @nochange
     * @post getSize()==getSize()@pre+1
     * */
    public void newItem(CompositeProduct compositeProduct){
        assert isWellFormed();
        int sizepre=menuItems.size();
        menuItems.add(compositeProduct);
        int sizepost=menuItems.size();
        assert sizepost==sizepre+1;
        assert isWellFormed();

    }

    /**
     * deletes an item from the menu
     * @pre getSize() greater than 0 and getSize() smaller than index
     * @post getSize()==getSize()@pre-1
     * */
    public void deleteItem(int index){
        int sizepre=menuItems.size();
        assert sizepre>0 && sizepre<index;
        menuItems.remove(index);
        int sizepost=menuItems.size();
        assert sizepost == sizepre-1;
    }

    public void setMenuItems(ArrayList<CompositeProduct> menuItems) {
        this.menuItems = menuItems;
    }

    /**
     * gets the items ordered from the hashMap
     * @pre containsKey()==true
     * @post @nochange
     * @return  item colection
     * */
    public Collection<CompositeProduct> getKey(Order order) {
        assert hashMap.containsKey(order)==true;
        return hashMap.get(order);
    }

    protected boolean isWellFormed(){
       int n=0;
       int count =0;

       if(hashMap.size()!=0)
           count++;
       else
           return  true;
       if(menuItems.size()==0)
           count++;

        return n == count;
    }
}
