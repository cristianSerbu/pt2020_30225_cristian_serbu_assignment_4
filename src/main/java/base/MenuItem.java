package base;

public abstract class MenuItem {
    abstract int computePrice();
}
