package base;

import java.util.ArrayList;
import java.util.Collection;

public interface IRestaurantProcessing {
    public void deleteItem(int index);
    public void newItem(CompositeProduct compositeProduct);
    public void setMenuItems(ArrayList<CompositeProduct> menuItems);
    public void addOrder(Order order, Collection<CompositeProduct> c);
}
