package base;

import view.ChefView;
import java.util.Observable;
import java.util.Observer;

public class OrderObserver implements Observer {
    private ChefView chefView;
    public OrderObserver(ChefView chefView) {
        this.chefView = chefView;
    }


    @Override
    public void update(Observable o, Object arg) {
        chefView.getComanda("comanda noua: "+ arg);
    }
}
