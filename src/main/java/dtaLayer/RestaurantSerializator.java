package dtaLayer;

import base.CompositeProduct;
import base.Restaurant;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class RestaurantSerializator {
    public static void main(String [] args) {
        Restaurant restaurant=new Restaurant();
        ArrayList<CompositeProduct> menu = new ArrayList<>();
        menu.add(new CompositeProduct("cafea",8));
        menu.add(new CompositeProduct("panini",10));
        menu.add(new CompositeProduct("supa de pui",10));
        menu.add(new CompositeProduct("pizza",13));
        menu.add(new CompositeProduct("coca-cola",5));
        menu.add(new CompositeProduct("sos iute panini",2));
        restaurant.setMenuItems(menu);
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("restaurant.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(restaurant);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in restaurant.ser");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
}
