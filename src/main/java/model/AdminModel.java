package model;

import base.CompositeProduct;
import base.Restaurant;

import java.util.ArrayList;

public class AdminModel {
private Restaurant restaurant;

    public void addMenuItem(CompositeProduct compositeProduct){ restaurant.newItem(compositeProduct); }

    public void deleteMenuItem(int index){ restaurant.deleteItem(index); }

    public AdminModel(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public ArrayList<CompositeProduct> getMenu(){
    return restaurant.getMenuItems();
    }

    public void setMenu(CompositeProduct c, int index){
       ArrayList<CompositeProduct> acp=restaurant.getMenuItems();
       acp.set(index,c);
       restaurant.setMenuItems(acp);
    }

}
