package model;

import base.CompositeProduct;
import base.Order;
import base.Restaurant;

import java.util.ArrayList;
import java.util.Collection;

public class WaiterModel {
    private Restaurant restaurant;


    public WaiterModel(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

     public void addOrder(Order o, Collection<CompositeProduct> c){ restaurant.addOrder(o,c); }

    public ArrayList<CompositeProduct> getMenu(){
        return restaurant.getMenuItems();
    }

    public Collection<CompositeProduct> getKey(Order order) {
        return restaurant.getKey(order);
    }

}
