package model;

import base.CompositeProduct;
import base.OrderObserver;
import base.Restaurant;

import java.util.ArrayList;

public class ChefModel {
    private Restaurant restaurant;

    public ChefModel(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }
}
