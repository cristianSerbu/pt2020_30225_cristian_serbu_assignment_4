package gui;

import base.Restaurant;
import controller.AdminCon;
import controller.ChefCon;
import controller.WaiterCon;
import model.AdminModel;
import model.ChefModel;
import model.WaiterModel;
import view.AdminView;
import view.ChefView;
import view.WaiterView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class MainClass {
    public static void main(String[] args) {
        Restaurant restaurant= null;

        try {
            FileInputStream fileIn = new FileInputStream("restaurant.ser");//args[0]
            ObjectInputStream in = new ObjectInputStream(fileIn);
            restaurant = (Restaurant) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Restaurant class not found");
            c.printStackTrace();
            return;
        }
        AdminModel adminModel= new AdminModel(restaurant);
        AdminView adminView= new AdminView();
        AdminCon adminCon= new AdminCon(adminView,adminModel);
        adminView.setVisible(true);
        WaiterView waiterView=new WaiterView();
        WaiterModel waiterModel=new WaiterModel(restaurant);
        WaiterCon waiterCon=new WaiterCon(waiterModel,waiterView);
        ChefModel chefModel = new ChefModel(restaurant);
        ChefView chefView = new ChefView();
        ChefCon chefCon = new ChefCon(chefView,chefModel);
        chefView.setVisible(true);
        waiterView.setVisible(true);
    }
}
