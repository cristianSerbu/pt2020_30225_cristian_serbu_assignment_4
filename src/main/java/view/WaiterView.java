package view;

import base.CompositeProduct;
import base.Order;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

public class WaiterView extends JFrame {

    private DefaultListModel listModel=new DefaultListModel();
    private DefaultListModel orderModel=new DefaultListModel();

    private JLabel titlu = new JLabel("PAGINA OSPATAR");
    private JTextField idb = new JTextField(5);
    private JTextField dateb = new JTextField(5);
    private JTextField tableb = new JTextField(5);
    private JButton copyP3 = new JButton("Adauga comanda");
    private JButton price = new JButton("calculeaza pretul");
    private JLabel id = new JLabel("id:");
    private JLabel date = new JLabel("date(day):");
    private JLabel table = new JLabel("table:");
    private JLabel pret1 = new JLabel("pretul comenzii:          lei");
    private JLabel pret2 = new JLabel("");
    private JLabel ordertitle = new JLabel("Order List");
    private JButton genereaza = new JButton("Chitanta");
    private JButton refresh = new JButton("refresh meniu");

    private JList meniul;
    private JList comanda;


    public WaiterView() {

        this.setTitle("Restaurant: Ospatar");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(800, 400);
        //adaugam elemente si le punem in frame cu spring layout
        SpringLayout layout = new SpringLayout();
        this.setLayout(layout);
        this.add(titlu);
        this.add(copyP3);
        this.add(id);
        this.add(date);
        this.add(table);
        this.add(idb);
        this.add(tableb);
        this.add(dateb);
        this.add(price);
        this.add(pret1);
        this.add(pret2);
        this.add(ordertitle);
        this.add(genereaza);
        this.add(refresh);
        meniul=new JList(listModel);
        this.add(meniul);
        meniul.setPreferredSize(new Dimension(260,250));
        meniul.setVisibleRowCount(10);
        comanda=new JList(orderModel);
        this.add(comanda);
        comanda.setPreferredSize(new Dimension(260,250));
        comanda.setVisibleRowCount(10);
        comanda.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        layout.putConstraint(SpringLayout.WEST, titlu, 340, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, copyP3, 10, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, copyP3, 330, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, id, 360, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, table, 360, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, date, 360, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, idb, 360, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, tableb, 360, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, dateb, 360, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, idb, 0, SpringLayout.SOUTH, id);
        layout.putConstraint(SpringLayout.NORTH, tableb, 0, SpringLayout.SOUTH, table);
        layout.putConstraint(SpringLayout.NORTH, dateb, 0, SpringLayout.SOUTH, date);
        layout.putConstraint(SpringLayout.NORTH, id, 50, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, date, 20, SpringLayout.SOUTH, id);
        layout.putConstraint(SpringLayout.NORTH, table, 20, SpringLayout.SOUTH, date);
        layout.putConstraint(SpringLayout.NORTH, meniul, 50, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, meniul, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, pret1, 280, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, pret1, 280, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, pret2, 280, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, pret2, 375, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, comanda, 50, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, comanda, 500, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, genereaza, 330, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, genereaza, 200, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, price, 330, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, price, 280, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, price, 330, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, price, 400, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, refresh, 20, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, refresh, 50, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, ordertitle, 20, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, ordertitle, 600, SpringLayout.WEST, this);
        //sfarsit de pus elemente
    }



    public void add3Listener(ActionListener listenForCalcButton) {
        copyP3.addActionListener(listenForCalcButton);
    }
    public void addpriceListener(ActionListener listenForCalcButton) {
        price.addActionListener(listenForCalcButton);
    }
    public void addGenereazaListener(ActionListener listenForCalcButton) { genereaza.addActionListener(listenForCalcButton); }
    public void addRefreshListener(ActionListener listenForCalcButton) { refresh.addActionListener(listenForCalcButton); }


    public void setList(ArrayList<CompositeProduct> t){
        listModel.removeAllElements();
        for(CompositeProduct a: t) {
            listModel.addElement(a.toString());
        }
    }

        public int[] getMenuItems() {
            int indexes[] = meniul.getSelectedIndices();
        return indexes;
        }
    public Order createOrder(){

        int id = Integer.parseInt(idb.getText());
        int date = Integer.parseInt(dateb.getText());
        int table = Integer.parseInt(tableb.getText());
        Order o = new Order(id,date,table);
        return o;
    }
    public void addOrder(Order o){
        orderModel.addElement(o.toString());
    }
    public int getOrder(){
        int index = comanda.getSelectedIndex();
        return index;
    }
    public void setPret(int pret){
        pret2.setText(Integer.toString(pret));
    }

}
