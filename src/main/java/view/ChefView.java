package view;
import javax.swing.*;

public class ChefView extends JFrame {

    private DefaultListModel listModel=new DefaultListModel();
    private JLabel titlu = new JLabel("PAGINA CHEF");

    private JLabel comanda = new JLabel("Asteapta sa apara o comanda, aici.");






    public ChefView() {

        this.setTitle("Restaurant: Chef");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600, 200);

        //adaugam elemente si le punem in frame cu spring layout
        SpringLayout layout = new SpringLayout();
        this.setLayout(layout);
        this.add(titlu);
        this.add(comanda);

        layout.putConstraint(SpringLayout.WEST, titlu, 200, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, comanda, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, comanda, 50, SpringLayout.NORTH, this);
        //sfarsit de pus elemente
    }

    public void getComanda(String comandaNoua){

        comanda.setText(comandaNoua);
    }

}
