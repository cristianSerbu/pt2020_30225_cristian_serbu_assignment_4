package view;

import base.CompositeProduct;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdminView extends JFrame {

    private DefaultListModel listModel=new DefaultListModel();
    private JLabel titlu = new JLabel("PAGINA ADMIN");
    private JTextField menuName = new JTextField(15);
    private JTextField menuPrice = new JTextField(5);
    private JButton copyP3 = new JButton("Adauga meniu");
    private JLabel nume = new JLabel("Nume produs:");
    private JLabel pret = new JLabel("Pret:");
    private JButton sterge = new JButton("Sterge selectia");
    private JButton editeaza = new JButton("Editeaza selectia");
    private JLabel help1 = new JLabel("adauga meniu: agauga datele introduse in lista");
    private JLabel help2 = new JLabel("inlocuieste elementul selectat cu datele din casuta");
    private JLabel help3 = new JLabel("sterge selectia: sterge din lista elementul selectat");

    private JList meniul;



    public AdminView() {

        this.setTitle("Restaurant: Admin");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600, 400);

        //adaugam elemente si le punem in frame cu spring layout
        SpringLayout layout = new SpringLayout();
        this.setLayout(layout);
        this.add(titlu);
        this.add(copyP3);
        this.add(menuName);
        this.add(menuPrice);
        this.add(nume);
        this.add(pret);
        this.add(sterge);
        this.add(editeaza);
        this.add(help1);
        this.add(help2);
        this.add(help3);
        meniul=new JList(listModel);
        this.add(meniul);
        meniul.setPreferredSize(new Dimension(260,250));
        meniul.setVisibleRowCount(10);
        meniul.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        layout.putConstraint(SpringLayout.NORTH, help1, 130, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, help2, 10, SpringLayout.SOUTH, help1);
        layout.putConstraint(SpringLayout.NORTH, help3, 10, SpringLayout.SOUTH, help2);
        layout.putConstraint(SpringLayout.WEST, help1, 290, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, help2, 290, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, help3, 290, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, editeaza, 330, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.EAST, editeaza, -10, SpringLayout.WEST, sterge);
        layout.putConstraint(SpringLayout.WEST, sterge, 160, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, sterge, 330, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, meniul, 20, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, meniul, 70, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, nume, 330, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.SOUTH, nume, 0, SpringLayout.NORTH, menuName);
        layout.putConstraint(SpringLayout.WEST, pret, 330, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.SOUTH, pret, 0, SpringLayout.NORTH, menuPrice);
        layout.putConstraint(SpringLayout.WEST, menuPrice, 330, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, menuPrice, 300, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, menuName, 250, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, menuName, 330, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, titlu, 380, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, copyP3, 10, SpringLayout.EAST, sterge);
        layout.putConstraint(SpringLayout.NORTH, copyP3, 330, SpringLayout.NORTH, this);
        //sfarsit de pus elemente
    }

    public CompositeProduct getMenu(){

        String menu= menuName.getText();
        String price=menuPrice.getText();
        int p = Integer.parseInt(price);
        CompositeProduct cp= new CompositeProduct(menu,p);
        return cp;
    }

    public void add3Listener(ActionListener listenForCalcButton) {
        copyP3.addActionListener(listenForCalcButton);
    }
    public void addStergeListener(ActionListener listenForCalcButton) {
        sterge.addActionListener(listenForCalcButton);
    }
    public void addEditListener(ActionListener listenForCalcButton) {
        editeaza.addActionListener(listenForCalcButton);
    }

    public void setList(ArrayList<CompositeProduct> t){
        for(CompositeProduct a: t) {
            listModel.addElement(a.toString());
        }
    }
    public void addToList(CompositeProduct t){

            listModel.addElement(t.toString());
        }

        public int deleteFromList(){

            int index = meniul.getSelectedIndex();
            listModel.remove(index);
            System.out.println(index);
        return index;
    }
    public int editFromList(CompositeProduct c){
        int index = meniul.getSelectedIndex();
        listModel.set(index, c.toString());
        return index;
    }

}
